﻿using System;
using System.Collections.Generic;
using movnetwork.Models;

namespace movnetwork
{
    public class MovieViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime ReleaseDate { get; set; }
        public Genre Genre { get; set; }
        public List<Actor> Actors { get; set; }
    }
}
