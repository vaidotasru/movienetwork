﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using movnetwork.DataContracts.Requests;
using movnetwork.Models;

namespace movnetwork.DataContracts
{
    public interface IActorRepository
    {
        Task<List<ActorViewModel>> GetAllActors();
        Task<ActorViewModel> GetActorById(int id);
        Task<Actor> AddActor(AddActorRequest request);
        Task EditActor(EditActorRequest request);
        Task DeleteActor(int id);
    }
}
