﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using movnetwork.DataContracts.Requests;
using movnetwork.Models;

namespace movnetwork.DataContracts
{
    public interface IGenreRepository
    {
        Task<List<GenreViewModel>> GetAllGenres();
        Task<GenreViewModel> GetGenreById(int id);
        Task<Genre> AddGenre(AddGenreRequest request);
        Task EditGenre(EditGenreRequest request);
        Task DeleteGenre(int id);
    }
}
