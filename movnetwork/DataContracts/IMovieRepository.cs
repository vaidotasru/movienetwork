﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using movnetwork.DataContracts.Requests;
using movnetwork.Models;

namespace movnetwork.DataContracts
{
    public interface IMovieRepository
    {
        Task<List<MovieViewModel>> GetAllMovies();
        Task<MovieViewModel> GetMovieById(int id);
        Task<Movie> AddMovie(AddMovieRequest request);
        Task EditMovie(EditMovieRequest request);
        Task DeleteMovie(int id);
    }
}
