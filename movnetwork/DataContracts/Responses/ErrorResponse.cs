﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movnetwork.DataContracts.Responses
{
    public class ErrorResponse
    {
        public string ErrorMessage { get; set; }
    }
}
