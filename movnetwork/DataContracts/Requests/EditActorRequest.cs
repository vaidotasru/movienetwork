﻿using System.ComponentModel.DataAnnotations;

namespace movnetwork.DataContracts.Requests
{
    public class EditActorRequest
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
    }
}
