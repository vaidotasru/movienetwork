﻿using System.ComponentModel.DataAnnotations;

namespace movnetwork.DataContracts.Requests
{
    public class AddGenreRequest
    {
        [Required]
        public string GenreName { get; set; }
    }
}
