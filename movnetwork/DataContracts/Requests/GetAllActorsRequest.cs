﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movnetwork.DataContracts.Requests
{
    public class GetAllActorsRequest
    {
        public string Query { get; set; }
    }
}
