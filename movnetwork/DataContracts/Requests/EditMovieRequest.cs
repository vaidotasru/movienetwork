﻿using System;
using System.ComponentModel.DataAnnotations;
using movnetwork.Models;

namespace movnetwork.DataContracts.Requests
{
    public class EditMovieRequest
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public DateTime ReleaseDate { get; set; }
        [Required]
        public int Genre { get; set; }
    }
}
