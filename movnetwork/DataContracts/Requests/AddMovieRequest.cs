﻿using System;
using System.ComponentModel.DataAnnotations;
using movnetwork.Models;

namespace movnetwork.DataContracts.Requests
{
    public class AddMovieRequest
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public DateTime ReleaseDate { get; set; }
        [Required]
        public int Genre { get; set; }
    }
}
