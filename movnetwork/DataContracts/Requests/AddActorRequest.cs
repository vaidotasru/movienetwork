﻿using System.ComponentModel.DataAnnotations;

namespace movnetwork.DataContracts.Requests
{
    public class AddActorRequest
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
    }
}
