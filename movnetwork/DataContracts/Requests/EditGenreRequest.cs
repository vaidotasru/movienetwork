﻿using System.ComponentModel.DataAnnotations;

namespace movnetwork.DataContracts.Requests
{
    public class EditGenreRequest
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string GenreName { get; set; }
    }
}
