import * as React from 'react';
import { Route } from 'react-router-dom';
import { Layout } from './components/Layout';
import Home from './components/Home';
import Actors from './components/Actors';
import Movies from './components/Movies';
import Genres from './components/Genres';

export const routes = <Layout>
    <Route exact path='/' component={ Home } />
    <Route path='/actors' component={Actors} />
    <Route path='/movies' component={Movies} />
    <Route path='/genres' component={Genres} />
</Layout>;
