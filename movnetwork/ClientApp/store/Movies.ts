import axios from "axios";
import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import {Genre} from './Genres';
import {Actor} from './Actors';

export interface MoviesState {
    movies: Movie[],
}

export interface Movie {
    id : number,
    title : string,
    releaseDate : Date,
    genre : Genre,
    actors : Actor[],
};

// ACTIONS

interface RequestMovieList {
    type: 'REQUEST_MOVIES';
    movies: any;
}

interface AddMovie {
    type: 'ADD_MOVIE';
    movie: string,
}

interface EditMovie {
    type: 'EDIT_MOVIE';
    movie: string;
}

interface DeleteMovie {
    type: 'DELETE_MOVIE';
    id: number;
}

type KnownAction = RequestMovieList | AddMovie | EditMovie | DeleteMovie;


export const actionCreators = {
    requestMovieList : (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        axios.get(
            '/api/movies',
            {
              withCredentials: true,
            }
          )
            .then(response => {
              dispatch({
                  movies: response.data,
                  type: 'REQUEST_MOVIES',
              });
            })
            .catch(function () {
              // add logic for handling errors in the future
            });
    },

    addMovie : (movie): AppThunkAction<KnownAction> => (dispatch) => {
        axios.post(
            '/api/movies',
            movie,
          )
            .then(response => {
              dispatch({
                  movie: response.data,
                  type: 'ADD_MOVIE',
              });
            })
            .catch(function () {
              // add logic for handling errors in the future
            });
    },

    editMovie : (editedMovie): AppThunkAction<KnownAction> => (dispatch) => {
        axios.put(
            '/api/movies',
            editedMovie,
          )
            .then(response => {
              dispatch({
                  movie: response.data,
                  type: 'EDIT_MOVIE',
              });
            })
            .catch(function () {
              // add logic for handling errors in the future
            });
    },

    deleteMovie : (id): AppThunkAction<KnownAction> => (dispatch) => {
        axios.delete(
            `/api/movies/${id}`,
          )
            .then(() => {
              dispatch({
                  id: id,
                  type: 'DELETE_MOVIE',
              });
            })
            .catch(function () {
              // add logic for handling errors in the future
            });
    },


}

// REDUCER


const unloadedState: MoviesState = { movies: []};

export const reducer: Reducer<MoviesState> = (state: MoviesState, incomingAction: Action) => {
    const action = incomingAction as KnownAction;

    switch (action.type) {
        case 'REQUEST_MOVIES':
          return {
              movies : action.movies,
          };
          case 'DELETE_MOVIE': 
          var el = state.movies.find(function(element) {
             return element.id == action.id;
          });
          return {
              movies : state.movies.filter(e => e!== el)
          }
    }
    return state || unloadedState;
}