import axios from "axios";
import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';

export interface ActorsState {
    actors: Actor[],
}

export interface Actor {
    id : number,
    firstName : string,
    lastName : string,
};

// ACTIONS

interface RequestActorList {
    type: 'REQUEST_ACTORS';
    actors: any;
}

interface AddActor {
    type: 'ADD_ACTOR';
    actor: string,
}

interface EditActor {
    type: 'EDIT_ACTOR';
    actor: string;
}

interface DeleteActor {
    type: 'DELETE_ACTOR';
    id: number;
}

type KnownAction = RequestActorList | AddActor | EditActor | DeleteActor ;


export const actionCreators = {
    requestActorList : (): AppThunkAction<KnownAction> => (dispatch) => {
        axios.get(
            '/api/actors',
            {
              withCredentials: true,
            }
          )
            .then(response => {
              dispatch({
                  actors: response.data,
                  type: 'REQUEST_ACTORS',
              });
            })
            .catch(function () {
              // add logic for handling errors in the future
            });
    },

    addActor : (actor): AppThunkAction<KnownAction> => (dispatch) => {
        axios.post(
            '/api/actors',
            actor,
          )
            .then(response => {
              dispatch({
                  actor: response.data,
                  type: 'ADD_ACTOR',
              });
            })
            .catch(function () {
              // add logic for handling errors in the future
            });
    },

    editActor : (editedActor): AppThunkAction<KnownAction> => (dispatch) => {
        axios.put(
            '/api/actors',
            editedActor,
          )
            .then(response => {
              dispatch({
                  actor: response.data,
                  type: 'EDIT_ACTOR',
              });
            })
            .catch(function () {
              // add logic for handling errors in the future
            });
    },

    deleteActor : (id): AppThunkAction<KnownAction> => (dispatch) => {
        axios.delete(
            `/api/actors/${id}`,
          )
            .then(() => {
              dispatch({
                  id: id,
                  type: 'DELETE_ACTOR',
              });
            })
            .catch(function () {
              // add logic for handling errors in the future
            });
    },

}

// REDUCER

const unloadedState: ActorsState = { actors: []};

export const reducer: Reducer<ActorsState> = (state: ActorsState, incomingAction: Action) => {
    const action = incomingAction as KnownAction;

    switch (action.type) {
        case 'REQUEST_ACTORS':
          return {
              actors : action.actors,
          };
          case 'DELETE_ACTOR': 
          var el = state.actors.find(function(element) {
             return element.id == action.id;
          });
          return {
              actors : state.actors.filter(e => e!== el)
          }
    }
    return state || unloadedState;
}