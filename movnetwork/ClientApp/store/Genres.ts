import axios from "axios";
import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';

export interface GenresState {
    genres: Genre[],
}

export interface Genre {
    id : number, 
    genreName : string,
};

// ACTIONS

interface RequestGenresList {
    type: 'REQUEST_GENRES';
    genres: any;
}

interface AddGenre {
    type: 'ADD_GENRE';
    genre: string,
}

interface EditGenre {
    type: 'EDIT_GENRE';
    genre: string;
}

interface DeleteGenre {
    type: 'DELETE_GENRE';
    id: number;
}

type KnownAction = RequestGenresList | AddGenre | EditGenre | DeleteGenre;


export const actionCreators = {
    requestGenresList : (): AppThunkAction<KnownAction> => (dispatch) => {
        axios.get(
            '/api/genres',
          )
            .then(response => {
              dispatch({
                  genres: response.data,
                  type: 'REQUEST_GENRES',
              });
            })
            .catch(function () {
              // add logic for handling errors in the future
            });
    },

    addGenre : (genre): AppThunkAction<KnownAction> => (dispatch) => {
        axios.post(
            '/api/genres',
            genre,
          )
            .then(response => {
              dispatch({
                  genre: response.data,
                  type: 'ADD_GENRE',
              });
            })
            .catch(function () {
              // add logic for handling errors in the future
            });
    },

    editGenre : (editedGenre): AppThunkAction<KnownAction> => (dispatch) => {
        axios.put(
            '/api/genres',
            editedGenre,
          )
            .then(response => {
              dispatch({
                  genre: response.data,
                  type: 'EDIT_GENRE',
              });
            })
            .catch(function () {
              // add logic for handling errors in the future
            });
    },

    deleteGenre : (id): AppThunkAction<KnownAction> => (dispatch) => {
        axios.delete(
            `/api/genres/${id}`,
          )
            .then(() => {
              dispatch({
                  id: id,
                  type: 'DELETE_GENRE',
              });
            })
            .catch(function () {
              // add logic for handling errors in the future
            });
    },

}

// REDUCER


const unloadedState: GenresState = { genres: []};

export const reducer: Reducer<GenresState> = (state: GenresState, incomingAction: Action) => {
    const action = incomingAction as KnownAction;

    switch (action.type) {
        case 'REQUEST_GENRES':
          return {
              genres : action.genres,
          };
        case 'DELETE_GENRE': 
          var el = state.genres.find(function(element) {
             return element.id == action.id;
          });
          return {
              genres : state.genres.filter(e => e!== el)
          }
    }
    return state || unloadedState;
}