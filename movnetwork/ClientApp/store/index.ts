import * as Actors from './Actors';
import * as Movies from './Movies';
import * as Genres from './Genres';

// The top-level state object
export interface ApplicationState {
    actors: Actors.ActorsState;
    movies: Movies.MoviesState;
    genres: Genres.GenresState;
}

// Whenever an action is dispatched, Redux will update each top-level application state property using
// the reducer with the matching name. It's important that the names match exactly, and that the reducer
// acts on the corresponding ApplicationState property type.
export const reducers = {
    actors: Actors.reducer,
    movies: Movies.reducer,
    genres: Genres.reducer,
};

// This type can be used as a hint on action creators so that its 'dispatch' and 'getState' params are
// correctly typed to match your store.
export interface AppThunkAction<TAction> {
    (dispatch: (action: TAction) => void, getState: () => ApplicationState): void;
}
