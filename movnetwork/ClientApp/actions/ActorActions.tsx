import axios from 'axios';

export const RECEIVE_ACTORS = "RECEIVE_ACTORS";

export const fetchActors = () => dispatch => {
    axios.get(
      'api/actors',
      {
        withCredentials: true,
      }
    )
      .then(response => {
        dispatch({
            users: response.data,
            type: RECEIVE_ACTORS,
        });
      })
      .catch(function () {
        // add logic for handling errors in the future
      });
  };
  