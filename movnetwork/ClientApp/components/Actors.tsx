import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import * as ActorsState from '../store/Actors';
import Modal from 'react-modal';

interface ActorsState {
    addModalIsOpen: boolean,
    editModalIsOpen: boolean,
    firstNameInputValue: string,
    lastNameInputValue,
    editId: number,
}

type ActorsProps =
    ActorsState.ActorsState        // ... state we've requested from the Redux store
    & typeof ActorsState.actionCreators      // ... plus action creators we've requested
    & RouteComponentProps<{}>;

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-70%, -70%)'
    }
};

class Actors extends React.Component<ActorsProps, ActorsState> {
    constructor(props) {
        super(props);

        this.state = {
            addModalIsOpen: false,
            editModalIsOpen: false,
            firstNameInputValue: "",
            lastNameInputValue: "",
            editId: 0,
        };

        this.openAddModal = this.openAddModal.bind(this);
        this.openEditModal = this.openEditModal.bind(this);
        this.submitNewActor = this.submitNewActor.bind(this);
        this.submitEditActor = this.submitEditActor.bind(this);
        this.closeAddModal = this.closeAddModal.bind(this);
        this.closeEditModal = this.closeEditModal.bind(this);
        this.removeActor = this.removeActor.bind(this);

    }

    componentWillMount() {
        this.props.requestActorList();
    }

    openAddModal() {
        this.setState({
            addModalIsOpen: true,
        });
    }

    openEditModal(firstName, lastName, id) {
        this.setState({
            editModalIsOpen: true,
            firstNameInputValue: firstName,
            lastNameInputValue: lastName,
            editId: id,
        });
    }

    closeAddModal() {
        this.setState({
            addModalIsOpen: false,
            firstNameInputValue: "",
            lastNameInputValue: "",
        });
    }

    closeEditModal() {
        this.setState({
            editModalIsOpen: false,
            firstNameInputValue: "",
            lastNameInputValue: "",
            editId: 0,
        });
    }

    updateFirstNameInputValue(event) {
        this.setState({
            firstNameInputValue: event.target.value,
        });
    }

    updateLastNameInputValue(event) {
        this.setState({
            lastNameInputValue: event.target.value,
        });
    }

    submitNewActor() {
        var newActor = {
            "FirstName": this.state.firstNameInputValue,
            "LastName": this.state.lastNameInputValue,
        };
        this.props.addActor(newActor);
    }

    submitEditActor() {
        var editedActor = {
            "FirstName": this.state.firstNameInputValue,
            "LastName": this.state.lastNameInputValue,
            "Id": this.state.editId,
        };
        this.props.editActor(editedActor);
    }

    removeActor(id) {
        this.props.deleteActor(id);
        this.forceUpdate();
    }


    public render() {

        return <div>
            {this.renderAddModal()}
            {this.renderEditModal()}
            {this.renderActorsTable()}

            <button
                className='.btn-default'
                onClick={this.openAddModal}
            >
                Add Actor
            </button>

        </div>;
    }

    private renderActorsTable() {
        return <table className='table'>
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
            </thead>
            <tbody>
                {this.props.actors.map(actor =>
                    <tr 
                      key={actor.id} 
                    >
                        <td onClick={() => this.openEditModal(actor.firstName,actor.lastName, actor.id)}>{actor.firstName}</td>
                        <td onClick={() => this.openEditModal(actor.firstName,actor.lastName, actor.id)}>{actor.lastName}</td>
                        <button onClick={() => this.removeActor(actor.id)}>
                            <span className="glyphicon glyphicon-remove remove-button"></span>
                        </button>
                    </tr>
                )}
            </tbody>
        </table>;
    }

    private renderAddModal() {
        return (
            <Modal
                isOpen={this.state.addModalIsOpen}
                onRequestClose={this.closeAddModal}
                style={customStyles}
            >
                <h4> Add New Actor </h4>
                <form>
                    <input
                        className='form-control'
                        id='firstnamefield'
                        onChange={event => this.updateFirstNameInputValue(event)}
                        type='text'
                        value={this.state.firstNameInputValue}
                    />
                    <input
                        className='form-control'
                        id='lastnamefield'
                        onChange={event => this.updateLastNameInputValue(event)}
                        type='text'
                        value={this.state.lastNameInputValue}
                    />

                    <button
                        type='submit'
                        className='btn btn-primary'
                        onClick={this.submitNewActor}
                    >
                        Submit
                    </button>
                </form>
            </Modal>
        )
    }

    private renderEditModal() {
        return (
            <Modal
                isOpen={this.state.editModalIsOpen}
                onRequestClose={this.closeEditModal}
                style={customStyles}
            >
                <h4> Edit Actor </h4>
                <form>
                    <input
                        className='form-control'
                        id='firstnamefield'
                        onChange={event => this.updateFirstNameInputValue(event)}
                        type='text'
                        value={this.state.firstNameInputValue}
                    />
                    <input
                        className='form-control'
                        id='lastnamefield'
                        onChange={event => this.updateLastNameInputValue(event)}
                        type='text'
                        value={this.state.lastNameInputValue}
                    />


                    <button
                        type='submit'
                        className='btn btn-primary'
                        onClick={this.submitEditActor}
                    >
                        Submit
                    </button>
                </form>
            </Modal>
        )
    }
}
export default connect(
    (state: ApplicationState) => state.actors, // Selects which state properties are merged into the component's props
    ActorsState.actionCreators                 // Selects which action creators are merged into the component's props
)(Actors) as typeof Actors;
