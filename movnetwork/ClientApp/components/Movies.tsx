import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import * as MoviesState from '../store/Movies';
import * as GenresState from '../store/Genres';
import * as ActorsState from '../store/Actors';
import Modal from 'react-modal';

interface MoviesState {
    addModalIsOpen: boolean,
    editModalIsOpen: boolean,
    inputTitleValue: string,
    inputReleaseDateValue: Date,
    inputGenreIdValue: number,
    editId: number,
    searchText: string,
    genreFilter: string,
    dateFromFilter: Date,
    dateToFilter: Date,
    actorsListOpen: boolean,
    actorsList: ActorsState.Actor[],
    movieActorsListId: number,
}

type MoviesProps =
    MoviesState.MoviesState
    & GenresState.GenresState
    & typeof MoviesState.actionCreators
    & typeof GenresState.actionCreators
    & RouteComponentProps<{}>;

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-70%, -70%)'
    }
};

class Movies extends React.Component<MoviesProps, MoviesState> {
    constructor(props) {
        super(props);
        this.state = {
            addModalIsOpen: false,
            editModalIsOpen: false,
            inputTitleValue: "",
            inputReleaseDateValue: null,
            inputGenreIdValue: 0,
            editId: 0,
            searchText: "",
            genreFilter: "All",
            dateFromFilter: null,
            dateToFilter: null,
            actorsListOpen: false,
            actorsList: null,
            movieActorsListId: 0,
        };

        this.openAddModal = this.openAddModal.bind(this);
        this.openEditModal = this.openEditModal.bind(this);
        this.submitNewMovie = this.submitNewMovie.bind(this);
        this.submitEditMovie = this.submitEditMovie.bind(this);
        this.closeAddModal = this.closeAddModal.bind(this);
        this.closeEditModal = this.closeEditModal.bind(this);
        this.removeMovie = this.removeMovie.bind(this);
        this.updateGenreInputValue = this.updateGenreInputValue.bind(this);
        this.isGenreSelected = this.isGenreSelected.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.handleGenreFilter = this.handleGenreFilter.bind(this);
        this.handleFromDateFilter = this.handleFromDateFilter.bind(this);
        this.handleToDateFilter = this.handleToDateFilter.bind(this);
        this.handleClearFilters = this.handleClearFilters.bind(this);
        this.closeActorList = this.closeActorList.bind(this);
    }

    componentWillMount() {
        this.props.requestGenresList();
        this.props.requestMovieList();
    }

    openAddModal() {
        this.setState({
            addModalIsOpen: true,
        });
    }

    openEditModal(title, date, genre, id) {
        this.setState({
            editModalIsOpen: true,
            inputTitleValue: title,
            inputReleaseDateValue: date,
            inputGenreIdValue: genre,
            editId: id,
        });
    }

    closeAddModal() {
        this.setState({
            addModalIsOpen: false,
            inputTitleValue: "",
            inputReleaseDateValue: null,
        });
    }

    closeEditModal() {
        this.setState({
            editModalIsOpen: false,
            inputTitleValue: "",
            inputReleaseDateValue: null,
            editId: 0,
        });
    }

    updateTitleInputValue(event) {
        this.setState({
            inputTitleValue: event.target.value,
        });
    }

    updateReleaseDateInputValue(event) {
        this.setState({
            inputReleaseDateValue: event.target.value,
        });
    }

    updateGenreInputValue(e) {
        this.setState({
            inputGenreIdValue: e.target.value,
        });
    }

    isGenreSelected(genre) {
        return (genre == this.state.inputGenreIdValue) ? true : false;
    }

    submitNewMovie() {
        var newMovie = {
            "Title": this.state.inputTitleValue,
            "ReleaseDate": this.state.inputReleaseDateValue,
            "Genre": this.state.inputGenreIdValue,
        };
        this.props.addMovie(newMovie);
    }

    submitEditMovie() {
        var editedMovie = {
            "Title": this.state.inputTitleValue,
            "ReleaseDate": this.state.inputReleaseDateValue,
            "Genre": this.state.inputGenreIdValue,
            "Id": this.state.editId,

        };
        this.props.editMovie(editedMovie);
    }

    removeMovie(id) {
        this.props.deleteMovie(id);
        this.forceUpdate();
    }

    handleSearch(event) {
        this.setState({
            searchText: event.target.value,
        });
    }

    handleGenreFilter(event) {
        this.setState({
            genreFilter: event.target.value,
        })
    }

    handleFromDateFilter(event) {
        this.setState({
            dateFromFilter: event.target.value,
        })
    }

    handleToDateFilter(event) {
        this.setState({
            dateToFilter: event.target.value,
        })
    }

    handleClearFilters() {
        this.setState({
            searchText: "",
            genreFilter: "All",
            dateFromFilter: null,
            dateToFilter: null,
        })
    }

    handleActorListOpen(actors, id) {
        this.setState({
            actorsListOpen: true,
            actorsList: actors,
            movieActorsListId: id,
        })
    }

    closeActorList() {
        this.setState({
            actorsListOpen: false,
            actorsList: null,
            movieActorsListId: 0,
        })
    }

    handleActorAddToMovie() {
        
    }

    public render() {
        var filteredMovies = this.props.movies;

        filteredMovies = filteredMovies.filter((movie) => {
            let movieTitle = movie.title.toLowerCase();
            return movieTitle.indexOf(
                this.state.searchText.toLowerCase()) !== -1
        });
        if (this.state.genreFilter != "All") {
            filteredMovies = filteredMovies.filter((movie) => {
                let movieGenre = movie.genre.genreName.toLowerCase();
                return movieGenre.indexOf(
                    this.state.genreFilter.toLowerCase()) !== -1
            });
        }

        if (this.state.dateFromFilter != null) {
            filteredMovies = filteredMovies.filter((movie) => {
                let releaseDate = movie.releaseDate;
                return this.state.dateFromFilter < releaseDate
            });
        }

        if (this.state.dateToFilter != null) {
            filteredMovies = filteredMovies.filter((movie) => {
                let releaseDate = movie.releaseDate;
                return this.state.dateToFilter > releaseDate
            });
        }
        return <div>
            <form>
                <input
                    type='text'
                    onChange={this.handleSearch}
                    placeholder="Search for movie"
                    value={this.state.searchText}
                />
            </form>

            <select name="genres" onChange={this.handleGenreFilter} value={this.state.genreFilter} >
                <option value="All">All</option>
                {this.props.genres.map(genre =>
                    <option value={genre.genreName}>
                        {genre.genreName}
                    </option>
                )}
            </select>

            <input
                type='date'
                onChange={event => this.handleFromDateFilter(event)}
            />

            <input
                type='date'
                onChange={event => this.handleToDateFilter(event)}
            />

            <button onClick={this.handleClearFilters}> Clear Filters </button>

            {this.renderMoviesTable(filteredMovies)}
            {this.renderAddModal()}
            {this.renderEditModal()}
            {this.renderActorListModal()}

            <button
                className='.btn-default'
                onClick={this.openAddModal}
            >
                Add new movie
            </button>
        </div>;
    }

    private renderMoviesTable(movies) {
        return <table className='table'>
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Release Date</th>
                    <th> Genre </th>
                </tr>
            </thead>
            <tbody>
                {movies.map(movie =>
                    <tr key={movie.id}>
                        <td onClick={() => this.openEditModal(movie.title, movie.releaseDate, movie.genre.id, movie.id)}>{movie.title}</td>
                        <td> {movie.releaseDate}</td>
                        <td> {movie.genre.genreName} </td>

                        <button
                            className="btn btn-info"
                            onClick={() => this.handleActorListOpen(movie.actors, movie.id)}
                        >
                            Actors
                        </button>

                        <button onClick={() => this.removeMovie(movie.id)}>
                            <span className="glyphicon glyphicon-remove remove-button"></span>
                        </button>
                    </tr>
                )}
            </tbody>
        </table>;
    }

    private renderAddModal() {
        return (
            <Modal
                isOpen={this.state.addModalIsOpen}
                onRequestClose={this.closeAddModal}
                style={customStyles}
            >
                <h4> Add New Movie </h4>
                <form>
                    <input
                        className='form-control'
                        id='titlefield'
                        onChange={event => this.updateTitleInputValue(event)}
                        type='text'
                        value={this.state.inputTitleValue}
                    />
                    <input
                        type='date'
                        onChange={event => this.updateReleaseDateInputValue(event)}
                    />

                    <select name="genres" onChange={this.updateGenreInputValue}>
                        <option value="" disabled selected>Select movie genre</option>
                        {this.props.genres.map(genre =>
                            <option value={genre.id}>
                                {genre.genreName}
                            </option>
                        )}

                    </select>

                    <button
                        type='submit'
                        className='btn btn-primary'
                        onClick={this.submitNewMovie}
                    >
                        Submit
                    </button>
                </form>
            </Modal>
        )
    }

    private renderEditModal() {
        return (
            <Modal
                isOpen={this.state.editModalIsOpen}
                onRequestClose={this.closeEditModal}
                style={customStyles}
            >
                <h4> Edit Movie </h4>
                <form>
                    <input
                        className='form-control'
                        id='genrefield'
                        onChange={event => this.updateTitleInputValue(event)}
                        type='text'
                        value={this.state.inputTitleValue}
                    />

                    <input
                        onChange={event => this.updateReleaseDateInputValue(event)}
                        type='date'
                    />

                    <select name="genres" onChange={this.updateGenreInputValue}>
                        {this.props.genres.map(genre =>
                            <option
                                value={genre.id}
                                selected={this.isGenreSelected(genre.id)}
                            >
                                {genre.genreName}
                            </option>
                        )}
                    </select>

                    <button
                        type='submit'
                        className='btn btn-primary'
                        onClick={this.submitEditMovie}
                    >
                        Submit
                    </button>
                </form>
            </Modal>
        )
    }

    public renderActorListModal() {
        if (this.state.actorsList != null) {
            return (
                <Modal
                    isOpen={this.state.actorsListOpen}
                    onRequestClose={this.closeActorList}
                    style={customStyles}
                >
                    <h2> List of actors </h2>
                    <table className='table'>
                        <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.actorsList.map(actor =>
                                <tr key={actor.id}>
                                    <td>{actor.firstName}</td>
                                    <td>{actor.lastName}</td>
                                </tr>
                            )}
                        </tbody>
                    </table>;

                    <button
                        type='submit'
                        className='btn btn-primary'
                    >
                        Add Actor
                    </button>
                </Modal>
            )
        }
    }

    public renderAddActorToMovieModal() {
        return (
            <Modal>

            </Modal>
        )
    }

}
export default connect(
    (state: ApplicationState) => ({ ...state.movies, ...state.genres }),
    ({ ...MoviesState.actionCreators, ...GenresState.actionCreators })
)(Movies) as typeof Movies;
