import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import * as GenresState from '../store/Genres';
import Modal from 'react-modal';

interface GenresState {
    addModalIsOpen: boolean,
    editModalIsOpen: boolean,
    inputValue: string,
    editId: number,
}

type GenresProps =
    GenresState.GenresState        // ... state we've requested from the Redux store
    & typeof GenresState.actionCreators      // ... plus action creators we've requested
    & RouteComponentProps<{}>;

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-70%, -70%)'
    }
};

class Genres extends React.Component<GenresProps, GenresState> {
    constructor(props) {
        super(props);

        this.state = {
            addModalIsOpen: false,
            editModalIsOpen: false,
            inputValue: "",
            editId: 0,
        };

        this.openAddModal = this.openAddModal.bind(this);
        this.openEditModal = this.openEditModal.bind(this);
        this.submitNewGenre = this.submitNewGenre.bind(this);
        this.submitEditGenre = this.submitEditGenre.bind(this);
        this.closeAddModal = this.closeAddModal.bind(this);
        this.closeEditModal = this.closeEditModal.bind(this);
        this.removeGenre = this.removeGenre.bind(this);
    }

    componentWillMount() {
        this.props.requestGenresList();
    }

    openAddModal() {
        this.setState({
            addModalIsOpen: true,
        });
    }

    openEditModal(genre, id) {
        this.setState({
            editModalIsOpen: true,
            inputValue: genre,
            editId: id,
        });
    }

    closeAddModal() {
        this.setState({
            addModalIsOpen: false,
            inputValue: "",
        });
    }

    closeEditModal() {
        this.setState({
            editModalIsOpen: false,
            inputValue: "",
            editId: 0,
        });
    }

    updateInputValue(event) {
        this.setState({
            inputValue: event.target.value,
        });
    }

    submitNewGenre() {
        var newGenre = {
            "GenreName": this.state.inputValue
        };
        this.props.addGenre(newGenre);
    }

    submitEditGenre() {
        var editedGenre = {
            "GenreName": this.state.inputValue,
            "Id": this.state.editId,
        };
        this.props.editGenre(editedGenre);
    }

    removeGenre(id) {
        this.props.deleteGenre(id);
        this.forceUpdate();
    }

    public render() {

        return <div>
            {this.renderAddModal()}
            {this.renderEditModal()}
            {this.renderGenresTable()}

            <button
                className='.btn-default'
                onClick={this.openAddModal}
            >
                Add new genre
            </button>
        </div>;
    }

    private renderGenresTable() {
        return <table className='table'>
            <thead>
                <tr>
                    <th>Genre Name</th>
                </tr>
            </thead>
            <tbody>
                {this.props.genres.map(genre =>
                    <tr key={genre.id}  >
                        <td onClick={() => this.openEditModal(genre.genreName, genre.id)}>{genre.genreName} </td>
                        <button onClick={() => this.removeGenre(genre.id)}>
                            <span className="glyphicon glyphicon-remove remove-button"></span>
                        </button>
                    </tr>
                )}
            </tbody>
        </table>;

    }

    private renderAddModal() {
        return (
            <Modal
                isOpen={this.state.addModalIsOpen}
                onRequestClose={this.closeAddModal}
                style={customStyles}
            >
                <h4> Add New Genre </h4>
                <form>
                    <input
                        className='form-control'
                        id='genrefield'
                        onChange={event => this.updateInputValue(event)}
                        type='text'
                        value={this.state.inputValue}
                    />

                    <button
                        type='submit'
                        className='btn btn-primary'
                        onClick={this.submitNewGenre}
                    >
                        Submit
                    </button>
                </form>
            </Modal>
        )
    }

    private renderEditModal() {
        return (
            <Modal
                isOpen={this.state.editModalIsOpen}
                onRequestClose={this.closeEditModal}
                style={customStyles}
            >
                <h4> Edit Genre </h4>
                <form>
                    <input
                        className='form-control'
                        id='genrefield'
                        onChange={event => this.updateInputValue(event)}
                        type='text'
                        value={this.state.inputValue}
                    />

                    <button
                        type='submit'
                        className='btn btn-primary'
                        onClick={this.submitEditGenre}
                    >
                        Submit
                    </button>
                </form>
            </Modal>
        )
    }
}
export default connect(
    (state: ApplicationState) => state.genres, // Selects which state properties are merged into the component's props
    GenresState.actionCreators                 // Selects which action creators are merged into the component's props
)(Genres) as typeof Genres;
