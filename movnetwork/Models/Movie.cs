﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace movnetwork.Models
{
    public class Movie
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime ReleaseDate { get; set; }
        public Genre Genre { get; set; }

        public virtual ICollection<ActorMovie> ActorMovies { get; } = new List<ActorMovie>();
    }
}