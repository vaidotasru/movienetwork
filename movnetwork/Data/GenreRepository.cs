﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using movnetwork.DataContracts;
using Microsoft.EntityFrameworkCore;
using movnetwork.DataContracts.Requests;
using movnetwork.Models;

namespace movnetwork.Data
{
    public class GenreRepository : IGenreRepository
    {
        private readonly AppDbContext _dbContext;

        public GenreRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Genre> AddGenre(AddGenreRequest request)
        {
            var item = new Genre
            {
                GenreName = request.GenreName,
            };

            _dbContext.Genres.Add(item);
            await _dbContext.SaveChangesAsync();

            return item;
        }

        public Task DeleteGenre(int id)
        {
            var item = _dbContext.Genres.First(g => g.Id == id);

            _dbContext.Remove(item);
            return _dbContext.SaveChangesAsync();
        }

        public Task EditGenre(EditGenreRequest request)
        {
            var item = _dbContext.Genres.First(g => g.Id == request.Id);
            item.GenreName = request.GenreName;

            return _dbContext.SaveChangesAsync();
        }

        public async Task<List<GenreViewModel>> GetAllGenres ()
        {
            return await _dbContext.Genres.Select(g => new GenreViewModel
            {
                Id = g.Id,
                GenreName = g.GenreName,
            }).ToListAsync();
        }

        public Task<GenreViewModel> GetGenreById(int id)
        {
            return _dbContext
              .Genres
              .Where(gr => gr.Id == id).Select(g => new GenreViewModel
              {
                  Id = g.Id,
                  GenreName = g.GenreName, 
              }).SingleOrDefaultAsync();
        }
    }
}
