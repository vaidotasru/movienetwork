﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using movnetwork.DataContracts;
using Microsoft.EntityFrameworkCore;
using movnetwork.DataContracts.Requests;
using movnetwork.Models;

namespace movnetwork.Data
{
    public class ActorRepository : IActorRepository
    {
        private readonly AppDbContext _dbContext;

        public ActorRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<ActorViewModel>> GetAllActors()
        {
            return await _dbContext.Actors.Select(a => new ActorViewModel
            {
                FirstName = a.FirstName,
                LastName = a.LastName,
                Id = a.Id
            }).ToListAsync();
        }

        public Task<ActorViewModel> GetActorById(int id)
        {
            return _dbContext
              .Actors
              .Where(ac => ac.Id == id).Select(a => new ActorViewModel
                {
                  FirstName = a.FirstName,
                  LastName = a.LastName,
                  Id = a.Id,
                }).SingleOrDefaultAsync();
        }

        public async Task<Actor> AddActor(AddActorRequest request)
        {
            var item = new Actor
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
            };

            _dbContext.Actors.Add(item);
            await _dbContext.SaveChangesAsync();

            return item;
        }

        public Task EditActor(EditActorRequest request)
        {
            var item = _dbContext.Actors.First(g => g.Id == request.Id);
            item.FirstName = request.FirstName;
            item.LastName = request.LastName;

            return _dbContext.SaveChangesAsync();
        }

        public Task DeleteActor(int id)
        {
            var item = _dbContext.Actors.First(g => g.Id == id);

            _dbContext.Remove(item);
            return _dbContext.SaveChangesAsync();
        }
    }
}
