﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using movnetwork.DataContracts;
using Microsoft.EntityFrameworkCore;
using movnetwork.DataContracts.Requests;
using movnetwork.Models;

namespace movnetwork.Data
{
    public class MovieRepository : IMovieRepository
    {
        private readonly AppDbContext _dbContext;

        public MovieRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Movie> AddMovie(AddMovieRequest request)
        {
            var item = new Movie
            {
                Title = request.Title,
                ReleaseDate = request.ReleaseDate,
                Genre = _dbContext.Genres.FirstOrDefault(g => g.Id == request.Genre)
        };

            _dbContext.Movies.Add(item);
            await _dbContext.SaveChangesAsync();

            return item;
        }

        public Task DeleteMovie(int id)
        {
            var item = _dbContext.Movies.First(g => g.Id == id);

            _dbContext.Remove(item);
            return _dbContext.SaveChangesAsync();
        }

        public Task EditMovie(EditMovieRequest request)
        {
            var item = _dbContext.Movies.First(m => m.Id == request.Id);
            item.Title = request.Title;
            item.ReleaseDate = request.ReleaseDate;
            item.Genre = _dbContext.Genres.FirstOrDefault(g => g.Id == request.Genre);

            return _dbContext.SaveChangesAsync();
        }

        public async Task<List<MovieViewModel>> GetAllMovies()
        {
            return await _dbContext.Movies.Select(m => new MovieViewModel
            {
                Id = m.Id,
                Title = m.Title,
                ReleaseDate = m.ReleaseDate,
                Genre = m.Genre,
                Actors = m.ActorMovies.Select(x => x.Actor).ToList()
            }).ToListAsync();
        }

        public Task<MovieViewModel> GetMovieById(int id)
        {
            return _dbContext
             .Movies
             .Where(mv => mv.Id == id).Select(m => new MovieViewModel
             {
                 Id = m.Id,
                 Title = m.Title,
                 ReleaseDate = m.ReleaseDate,
                 Genre = m.Genre,
             }).SingleOrDefaultAsync();
        }
    }
}
