﻿using System;
using System.Collections.Generic;
using System.Linq;
using movnetwork.Models;
using System.Threading.Tasks;

namespace movnetwork.Data
{
    public class DbInitializer
    {
        public static async Task Initialize(AppDbContext context)
        {
            if (context.Genres.Any() || context.Actors.Any() || context.Movies.Any())
            {
                return;
            }

            var genres = new List<Genre>
            {
                new Genre{GenreName="Drama"},
                new Genre{GenreName="Comedy"},
                new Genre{GenreName="Action"},
            };

            genres.ForEach(g => context.Genres.Add(g));

            context.SaveChanges();

            var movies = new List<Movie>
            {
                new Movie{Title="Deadpool", ReleaseDate=DateTime.Parse("2016-01-21"), Genre=context.Genres.FirstOrDefault(g => g.GenreName == "Action")},
                new Movie{Title="Avengers Infinity War", ReleaseDate=DateTime.Parse("2018-04-23"), Genre=context.Genres.FirstOrDefault(g => g.GenreName == "Action")},
                new Movie{Title="Titanic", ReleaseDate=DateTime.Parse("1997-11-18"), Genre=context.Genres.FirstOrDefault(g => g.GenreName == "Drama")},
                new Movie{Title="American Pie", ReleaseDate=DateTime.Parse("1999-07-09"), Genre=context.Genres.FirstOrDefault(g => g.GenreName == "Action")},
            };

            var actors = new List<Actor>
            {
                new Actor{FirstName="Leonardo", LastName="DiCaprio"},
                new Actor{FirstName="Ryan", LastName="Reynolds"},
                new Actor{FirstName="Tom", LastName="Hanks"},
                new Actor{FirstName="Tom", LastName="Cruise"},
                new Actor{FirstName="Robert", LastName="Downey Jr."},
                new Actor{FirstName="Will", LastName="Smith"},
                new Actor{FirstName="Johny", LastName="Depp"},

            };

            context.AddRange(
                new ActorMovie { Movie = movies[0], Actor = actors[0] },
                new ActorMovie { Movie = movies[0], Actor = actors[1] },
                new ActorMovie { Movie = movies[0], Actor = actors[2] },
                new ActorMovie { Movie = movies[1], Actor = actors[1] },
                new ActorMovie { Movie = movies[1], Actor = actors[4] },
                new ActorMovie { Movie = movies[2], Actor = actors[6] },
                new ActorMovie { Movie = movies[2], Actor = actors[5] },
                new ActorMovie { Movie = movies[2], Actor = actors[3] },
                new ActorMovie { Movie = movies[3], Actor = actors[4] },
                new ActorMovie { Movie = movies[3], Actor = actors[5] }
                );

            movies.ForEach(m => context.Movies.Add(m));
            actors.ForEach(a => context.Actors.Add(a));

            context.SaveChanges();

        }
    }
}