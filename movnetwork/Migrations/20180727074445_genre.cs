﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace movnetwork.Migrations
{
    public partial class genre : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Movies_Genres_MovieGenreID",
                table: "Movies");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Movies",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "MovieGenreID",
                table: "Movies",
                newName: "GenreId");

            migrationBuilder.RenameIndex(
                name: "IX_Movies_MovieGenreID",
                table: "Movies",
                newName: "IX_Movies_GenreId");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Genres",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Actors",
                newName: "Id");

            migrationBuilder.AddColumn<int>(
                name: "ID",
                table: "ActorsMovies",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_Movies_Genres_GenreId",
                table: "Movies",
                column: "GenreId",
                principalTable: "Genres",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Movies_Genres_GenreId",
                table: "Movies");

            migrationBuilder.DropColumn(
                name: "ID",
                table: "ActorsMovies");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Movies",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "GenreId",
                table: "Movies",
                newName: "MovieGenreID");

            migrationBuilder.RenameIndex(
                name: "IX_Movies_GenreId",
                table: "Movies",
                newName: "IX_Movies_MovieGenreID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Genres",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Actors",
                newName: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Movies_Genres_MovieGenreID",
                table: "Movies",
                column: "MovieGenreID",
                principalTable: "Genres",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
