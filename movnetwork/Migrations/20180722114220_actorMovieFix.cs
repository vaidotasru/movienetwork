﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace movnetwork.Migrations
{
    public partial class actorMovieFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActorsMovies_Actors_ActorID",
                table: "ActorsMovies");

            migrationBuilder.DropForeignKey(
                name: "FK_ActorsMovies_Movies_MovieID",
                table: "ActorsMovies");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ActorsMovies",
                table: "ActorsMovies");

            migrationBuilder.DropIndex(
                name: "IX_ActorsMovies_ActorID",
                table: "ActorsMovies");

            migrationBuilder.DropColumn(
                name: "ID",
                table: "ActorsMovies");

            migrationBuilder.RenameColumn(
                name: "MovieID",
                table: "ActorsMovies",
                newName: "MovieId");

            migrationBuilder.RenameColumn(
                name: "ActorID",
                table: "ActorsMovies",
                newName: "ActorId");

            migrationBuilder.RenameIndex(
                name: "IX_ActorsMovies_MovieID",
                table: "ActorsMovies",
                newName: "IX_ActorsMovies_MovieId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ActorsMovies",
                table: "ActorsMovies",
                columns: new[] { "ActorId", "MovieId" });

            migrationBuilder.AddForeignKey(
                name: "FK_ActorsMovies_Actors_ActorId",
                table: "ActorsMovies",
                column: "ActorId",
                principalTable: "Actors",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ActorsMovies_Movies_MovieId",
                table: "ActorsMovies",
                column: "MovieId",
                principalTable: "Movies",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActorsMovies_Actors_ActorId",
                table: "ActorsMovies");

            migrationBuilder.DropForeignKey(
                name: "FK_ActorsMovies_Movies_MovieId",
                table: "ActorsMovies");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ActorsMovies",
                table: "ActorsMovies");

            migrationBuilder.RenameColumn(
                name: "MovieId",
                table: "ActorsMovies",
                newName: "MovieID");

            migrationBuilder.RenameColumn(
                name: "ActorId",
                table: "ActorsMovies",
                newName: "ActorID");

            migrationBuilder.RenameIndex(
                name: "IX_ActorsMovies_MovieId",
                table: "ActorsMovies",
                newName: "IX_ActorsMovies_MovieID");

            migrationBuilder.AddColumn<int>(
                name: "ID",
                table: "ActorsMovies",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ActorsMovies",
                table: "ActorsMovies",
                column: "ID");

            migrationBuilder.CreateIndex(
                name: "IX_ActorsMovies_ActorID",
                table: "ActorsMovies",
                column: "ActorID");

            migrationBuilder.AddForeignKey(
                name: "FK_ActorsMovies_Actors_ActorID",
                table: "ActorsMovies",
                column: "ActorID",
                principalTable: "Actors",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ActorsMovies_Movies_MovieID",
                table: "ActorsMovies",
                column: "MovieID",
                principalTable: "Movies",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
