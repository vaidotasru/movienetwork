﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using movnetwork.DataContracts;
using movnetwork.DataContracts.Requests;
using movnetwork.DataContracts.Responses;


namespace movnetwork.Controllers
{
    [Route("api/actors")]
    public class ActorsController : Controller
    {
        private readonly IActorRepository _repository;

        public ActorsController(IActorRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public Task<List<ActorViewModel>> GetAll()
        {
            return _repository.GetAllActors();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                var actor = await _repository.GetActorById(id);
                return Ok(actor);
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddActor([FromBody] AddActorRequest request)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var item = await _repository.AddActor(request);

                    return CreatedAtAction(nameof(GetById), new { id = item.Id }, item);
                }
                catch (ArgumentException e)
                {
                    return StatusCode(
                        (int)HttpStatusCode.Conflict,
                        new ErrorResponse { ErrorMessage = e.Message }
                    );
                }
            }

            return BadRequest(ModelState);
        }

        public async Task<IActionResult> EditActor(int id, [FromBody]EditActorRequest request)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _repository.EditActor(request);

                    return NoContent();
                }
                catch (InvalidOperationException)
                {
                    return NotFound();
                }
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteActor([FromRoute] int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _repository.DeleteActor(id);

                    return NoContent();
                }
                catch (InvalidOperationException)
                {
                    return NotFound();
                }
            }

            return BadRequest(ModelState);
        }
    }
}