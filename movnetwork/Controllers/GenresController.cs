﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using movnetwork.DataContracts;
using movnetwork.DataContracts.Requests;
using movnetwork.DataContracts.Responses;


namespace movnetwork.Controllers
{
    [Route("api/genres")]
    public class GenresController : Controller
    {
        private readonly IGenreRepository _repository;

        public GenresController(IGenreRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public Task<List<GenreViewModel>> GetAll()
        {
            return _repository.GetAllGenres();
        }
   

    [HttpGet("{id}")]
    public async Task<IActionResult> GetById(int id)
    {
        try
        {
            var movie = await _repository.GetGenreById(id);
            return Ok(movie);
        }
        catch (InvalidOperationException)
        {
            return NotFound();
        }
    }

        [HttpPost]
        public async Task<IActionResult> AddGenre([FromBody] AddGenreRequest request)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var item = await _repository.AddGenre(request);

                    return CreatedAtAction(nameof(GetById), new { id = item.Id }, item);
                }
                catch (ArgumentException e)
                {
                    return StatusCode(
                        (int)HttpStatusCode.Conflict,
                        new ErrorResponse { ErrorMessage = e.Message }
                    );
                }
            }

            return BadRequest(ModelState);
        }

        public async Task<IActionResult> EditGenre(int id, [FromBody]EditGenreRequest request)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _repository.EditGenre(request);

                    return NoContent();
                }
                catch (InvalidOperationException)
                {
                    return NotFound();
                }
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGenre([FromRoute] int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _repository.DeleteGenre(id);

                    return NoContent();
                }
                catch (InvalidOperationException)
                {
                    return NotFound();
                }
            }

            return BadRequest(ModelState);
        }
    }
}