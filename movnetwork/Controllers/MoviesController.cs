﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using movnetwork.DataContracts;
using movnetwork.DataContracts.Requests;
using movnetwork.DataContracts.Responses;

namespace movnetwork.Controllers
{
    [Route("api/movies")]
    public class MoviesController : Controller
    {
        private readonly IMovieRepository _repository;

        public MoviesController(IMovieRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public Task<List<MovieViewModel>> GetAll()
        {
            return _repository.GetAllMovies();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                var movie = await _repository.GetMovieById(id);
                return Ok(movie);
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
        }
        [HttpPost]
        public async Task<IActionResult> AddMovie([FromBody] AddMovieRequest request)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var item = await _repository.AddMovie(request);

                    return CreatedAtAction(nameof(GetById), new { id = item.Id }, item);
                }
                catch (ArgumentException e)
                {
                    return StatusCode(
                        (int)HttpStatusCode.Conflict,
                        new ErrorResponse { ErrorMessage = e.Message }
                    );
                }
            }

            return BadRequest(ModelState);
        }

        public async Task<IActionResult> EditMovie(int id, [FromBody]EditMovieRequest request)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _repository.EditMovie(request);

                    return NoContent();
                }
                catch (InvalidOperationException)
                {
                    return NotFound();
                }
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie([FromRoute] int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _repository.DeleteMovie(id);

                    return NoContent();
                }
                catch (InvalidOperationException)
                {
                    return NotFound();
                }
            }

            return BadRequest(ModelState);
        }
    }

}